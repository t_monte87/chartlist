import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router'

import { AppMaterialModule } from './app-material/app-material.module';
import { AppComponent } from './app.component';

import { GarageComponent, DialogOverviewExampleDialog} from './garage/garage.component';
import { RosterComponent } from './roster/roster.component';
import { dataService } from './data.service';
import { GroupByPipe } from './group-by.pipe';
import { OrderBy } from './order-by.pipe';
import { NavbarComponent } from './navbar/navbar.component';


//import { AppRoutingModule } from './app.routing.module';

const appRoutes: Routes = [
  { path: '', component: GarageComponent },
  { path: 'garage', component: GarageComponent },
  { path: 'roster', component: RosterComponent },
  { path: 'testing', component: NavbarComponent },  
];

@NgModule({
  declarations: [
    AppComponent,
    GarageComponent,
    RosterComponent,
    GroupByPipe,
    OrderBy,
    NavbarComponent,
    DialogOverviewExampleDialog
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppMaterialModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [dataService],
  bootstrap: [AppComponent],
  entryComponents: [DialogOverviewExampleDialog]
})
export class AppModule { }
