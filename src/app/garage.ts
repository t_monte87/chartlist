export interface Garage {
    chartNum: string,
    homeLocation: string,
    name: string,
    seniorityDate: string,
    promotionDate: string,
    hireDate: string,
    listNumber: number,
    chartDate: string,
    chartsMade: number,
    isCC: boolean,
    isMDA: boolean,
    isSick: boolean,
    isVC: boolean,
    isLodi: boolean
}