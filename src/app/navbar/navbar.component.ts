import { Component, OnInit } from '@angular/core';
import { AppMaterialModule } from '../app-material/app-material.module';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import { dataService } from '../data.service'
import { GroupByPipe } from '../group-by.pipe'
import {FormControl} from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
	selector: 'app-navbar',
	templateUrl: './navbar.component.html',
	styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

	url = '/personnel-query/api/byLocationAndDate?location=';
	refData = '/reference-data/referencedata/locations?date=2018-06-22';
	locations;
	posts;
	selectedDistrict;
	dateSelect: Date;
	public garage: any[];
	public filteredArray: any[];
	date = new FormControl(new Date());
	displayedColumns = ['name'];
	dataSource;

	constructor(private dataService: dataService, private http: HttpClient) { }
	ngOnInit() {
	this.getLocations();
	}
			
  getData() {
  this.http.get(this.url + this.selectedDistrict).subscribe(res => {
   this.dataSource = res;
   console.log(this.dataSource)
  });


}

  getLocations() {
  this.http.get(this.refData).subscribe(res => {
   this.locations = res;
   console.log(this.locations)
  });
}

}

// let filteredArray = arrayOfElements.filter((element) => 
//     element.subElements.some((subElement) => subElement.surname === 1))
//   .map(element => {
//     let newElt = Object.assign({}, element); // copies element
//     return newElt.subElements.filter(subElement => subElement.surName === '1');
//   });