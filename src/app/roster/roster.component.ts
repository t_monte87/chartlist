import { Component, OnInit } from '@angular/core';
import { GarageComponent } from '../garage/garage.component';
import { AppMaterialModule } from '../app-material/app-material.module';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';

import { dataService } from '../data.service'
import { GroupByPipe } from '../group-by.pipe'
import {FormControl} from '@angular/forms';

@Component({
	selector: 'app-roster',
	templateUrl: './roster.component.html',
	styleUrls: ['./roster.component.css']
})
export class RosterComponent implements OnInit {
	public dateSelect: Date;
	public garage: any[];
	public filteredArray: any[];
	public date = new FormControl(new Date());
	public groupedData: any[];
	public jsonDataKeys : any=[];
	public sDate: any;
	public eDate: any;

	constructor(private dataService: dataService) {
	}

	ngOnInit() {
		this.garage = this.dataService.garage;
		this.dateSelect	= new Date()
		this.filterByDate();
}


addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
	this.dateSelect = event.value;
	this.filterByDate()
}	

filterByDate() {
	
	let dateSelect = new Date(this.dateSelect);
	let dayOfWeek = dateSelect.getDay();
	let numDay = dateSelect.getDate();
	
	let sDate = new Date(dateSelect);
	sDate.setDate(numDay - dayOfWeek);
	sDate.setHours(0, 0, 0, 0);

	let eDate = new Date(dateSelect);
	eDate.setDate(numDay + (7 - dayOfWeek));
	eDate.setHours(0, 0, 0, 0);

	this.filteredArray = this.filterByDates(this.garage,sDate,eDate)
	this.filteredArray = this.filteredArray.sort((a, b) =>   a.isCC - b.isCC || +new Date(a.chartsMade) - +new Date(b.chartsMade) || +new Date(a.seniorityDate) - +new Date(b.seniorityDate) || a.listNumber - b.listNumber )		
	this.groupBy()
} 

groupBy(){
	var groups = {};
	for (var i = 0; i < this.filteredArray.length; i++) {
		if (this.filteredArray[i].chartDate === undefined || this.filteredArray[i].chartDate.length == 0) {
		var groupName = ""
		if (!groups[groupName]) {
			groups[groupName] = [];
		}
		groups[groupName].push(this.filteredArray[i]);
	}
		else { 
		var groupName: string = this.filteredArray[i].chartDate[0].date;
		if (!groups[groupName]) {
			groups[groupName] = [];
		}
		groups[groupName].push(this.filteredArray[i]);
	}

	}

	var myArray = [];
	for (groupName in groups) {
		myArray.push({key: groupName, value: groups[groupName]});
	}

	myArray = myArray.sort((a, b) =>  +new Date(a.key) - +new Date(b.key))
	this.groupedData = myArray

}

filterByDates = (data, start, end) => {
	var result = [];
	data.slice().forEach((d)=> {
		d.chartDate = [];


		d.chartDates.forEach((td) => {
			if(new Date(td.date) >= new Date(start) && new Date(td.date) <= new Date(end)) {
				d.chartDate.push(td);
				if(td.ccFrom) {
					d.isCC = true;
				}
				else {
					d.isCC = false;
				}
			}
		});   
		result.push(d); 
	});
	return result;
}

}



