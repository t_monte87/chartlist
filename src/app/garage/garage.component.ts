import { Component, OnInit, ViewChild, Inject, Input } from '@angular/core';
import { AppMaterialModule } from '../app-material/app-material.module';
import { dataService } from '../data.service'
import { Garage } from '../garage'
//import { Http } from '@angular/http';
import { MatTableDataSource } from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {FormControl} from '@angular/forms';


@Component({
  selector: 'app-garage',
  templateUrl: './garage.component.html',
  styleUrls: ['./garage.component.scss']
})


export class GarageComponent implements OnInit {
  public garage: any[]
  //   'status', 'quals', 'senority', 'phoneNum',
  displayedColumns = ['name', 'refNum', 'badge', 'section',  'chartNum', 'homeLocation', 'quals', 'chartsMade', ];
  ds;
  data: any;
  workerDetails: any;

  constructor(private dataService: dataService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.garage = this.dataService.garage;
    this.ds = new MatTableDataSource(this.garage);
    this.ds.paginator = this.paginator;
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.ds.filter = filterValue;
  }

  workerInfo(worker:any, i:any): void {
    let dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '1080px',
      height: '935px',
      data: {chartDates: worker.chartDates, isMDA: worker.isMDA, chartsMade: worker.chartsMade, perInfo: worker}


    });

    console.log(i)

    dialogRef.afterClosed().subscribe(result => {

      if (result == undefined){
        console.log("Nothing To Log")
      } else {
        this.garage[i].chartsMade = result.chartsMade
        this.garage[i].chartDates = result.chartDates
        this.garage[i].isMDA = result.isMDA
        console.log(worker)}
      });
  }


}



@Component({
  selector: 'user-info-dialog',
  templateUrl: 'user-info-dialog.html',
  styleUrls: ['user-info-dialog.scss'],
})
export class DialogOverviewExampleDialog {

  public info: any;
  uTable;
  uTabledisplayedColumns = ['date','ccFor','cancel'];
  data;

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data1: any) { this.data = Object.assign({},data1)}

  ngOnInit() {
    console.log(this.data)
    this.uTable = new MatTableDataSource(this.data.chartDates)

  }

  onChangeMDA(e, d: any) {

    if (e.checked == true) {
      this.data.isMDA = true;
    } else {
      this.data.isMDA = false;
    }
  }

  onToggleChart(e, d, i: any) {
    console.log(d)
    if (e.checked == true) {
      this.data.chartsMade = this.data.chartsMade + 1;
      this.data.chartDates = this.data.chartDates.map((c, index) => index === i ? Object.assign({}, c, {cancelled: true}) : c)
        // this.data.chartDates[i] = Object.assign({}, this.data.chartDates[i], {cancelled: true});
      } else {
        this.data.chartsMade = this.data.chartsMade - 1;
        this.data.chartDates = this.data.chartDates.map((c, index) => index === i ? Object.assign({}, c, {cancelled: false}) : c)
        // this.data.chartDates[i] = Object.assign({}, this.data.chartDates[i], {cancelled: false});
      }
    }

    addChart() {
      this.data.chartDates.push({date: "", ccFrom: "", cancelled: false})
      this.uTable = new MatTableDataSource(this.data.chartDates)
    }


    onNoClick(): void {
      this.dialogRef.close();
    }

    

    }





