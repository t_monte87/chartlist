import { Injectable } from '@angular/core';

@Injectable()
export class dataService {

  public garage: any[] =[

    {
    "chartNum": "F2",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "144137",
    "name": "A. Auguste Jr",
    "seniorityDate": "2013-11-10T05:00:00.000Z",
    "promotionDate": "2013-11-10T05:00:00.000Z",
    "hireDate": "2013-11-11T05:00:00.000Z",
    "listNumber": 3707,
    "chartDates": [ {"date":"2018-06-04T04:00:00.000Z", "ccFrom":"2018-06-05T04:00:00.000Z"}, {"date":"2018-06-12T04:00:00.000Z"}, {"date":"2018-06-20T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1790",
    "qualifications" : [ "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-120-4990"
    }
  },
  {
    "chartNum": "F2",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "194372",
    "name": "A. Cherry",
    "seniorityDate": "1995-07-30T05:00:00.000Z",
    "promotionDate": "1995-07-30T05:00:00.000Z",
    "hireDate": "1995-07-31T05:00:00.000Z",
    "listNumber": 1448,
    "chartDates": [ {"date":"2018-06-04T04:00:00.000Z"}, {"date":"2018-06-12T04:00:00.000Z"}, {"date":"2018-06-20T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1360",
    "qualifications" : [ "EZP" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-358-5417"
    }
  },
  {
    "chartNum": "F2",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "113419",
    "name": "C. Brigantti",
    "seniorityDate": "1988-07-17T05:00:00.000Z",
    "promotionDate": "1988-07-17T05:00:00.000Z",
    "hireDate": "1988-07-18T05:00:00.000Z",
    "listNumber": 588,
    "chartDates": [ {"date":"2018-06-04T04:00:00.000Z"}, {"date":"2018-06-12T04:00:00.000Z"}, {"date":"2018-06-20T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": true,
    "isSick": true,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2600",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-432-1434"
    }
  },
  {
    "chartNum": "F2",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "174983",
    "name": "F. Odumbo",
    "seniorityDate": "2015-09-06T04:00:00.000Z",
    "promotionDate": "2015-09-06T04:00:00.000Z",
    "hireDate": "2015-09-06T04:00:00.000Z",
    "listNumber": 6273,
    "chartDates": [ {"date":"2018-06-04T04:00:00.000Z"}, {"date":"2018-06-12T04:00:00.000Z"}, {"date":"2018-06-20T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3385",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-387-9934"
    }
  },
  {
    "chartNum": "F2",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "131730",
    "name": "G. Trabolse",
    "seniorityDate": "2005-12-04T06:00:00.000Z",
    "promotionDate": "2005-12-04T06:00:00.000Z",
    "hireDate": "2005-12-05T06:00:00.000Z",
    "listNumber": 4086,
    "chartDates": [ {"date":"2018-06-04T04:00:00.000Z"}, {"date":"2018-06-12T04:00:00.000Z"}, {"date":"2018-06-20T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1830",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-218-1822"
    }
  },
  {
    "chartNum": "F2",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "185595",
    "name": "S. Callender",
    "seniorityDate": "2014-08-17T04:00:00.000Z",
    "promotionDate": "2014-08-17T04:00:00.000Z",
    "hireDate": "2014-08-18T04:00:00.000Z",
    "listNumber": 4722,
    "chartDates": [ {"date":"2018-06-04T04:00:00.000Z"}, {"date":"2018-06-12T04:00:00.000Z"}, {"date":"2018-06-20T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1301",
    "qualifications" : [ "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-453-6092"
    }
  },
  {
    "chartNum": "SW-1",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "101286",
    "name": "I. Geiger",
    "seniorityDate": "2004-09-12T04:00:00.000Z",
    "promotionDate": "2004-09-12T04:00:00.000Z",
    "hireDate": "2004-09-13T04:00:00.000Z",
    "listNumber": 1568,
    "chartDates": [ {"date":"2018-06-04T04:00:00.000Z"}, {"date":"2018-06-12T04:00:00.000Z"}, {"date":"2018-06-20T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2822",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-452-6948"
    }
  },
  {
    "chartNum": "SW-1",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "103869",
    "name": "J. Hojnacki",
    "seniorityDate": "2006-10-15T05:00:00.000Z",
    "promotionDate": "2006-10-15T05:00:00.000Z",
    "hireDate": "2006-10-16T05:00:00.000Z",
    "listNumber": 6211,
    "chartDates": [ {"date":"2018-06-04T04:00:00.000Z"}, {"date":"2018-06-12T04:00:00.000Z"}, {"date":"2018-06-20T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3340",
    "qualifications" : [ "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-666-1500"
    }
  },
  {
    "chartNum": "SW-1",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "164802",
    "name": "J. Mejia",
    "seniorityDate": "1999-09-19T04:00:00.000Z",
    "promotionDate": "1999-09-19T04:00:00.000Z",
    "hireDate": "1999-09-20T04:00:00.000Z",
    "listNumber": 12,
    "chartDates": [ {"date":"2018-06-04T04:00:00.000Z"}, {"date":"2018-06-12T04:00:00.000Z"}, {"date":"2018-06-20T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2701",
    "qualifications" : [ "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-629-9107"
    }
  },
  {
    "chartNum": "SW-1",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "130011",
    "name": "M. Whittingham",
    "seniorityDate": "1990-02-04T06:00:00.000Z",
    "promotionDate": "1990-02-04T06:00:00.000Z",
    "hireDate": "1990-02-05T06:00:00.000Z",
    "listNumber": 7113,
    "chartDates": [ {"date":"2018-06-04T04:00:00.000Z"}, {"date":"2018-06-12T04:00:00.000Z"}, {"date":"2018-06-20T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2664",
    "qualifications" : [ "EZP" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-710-6621"
    }
  },
  {
    "chartNum": "SW-11",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "170908",
    "name": "C. DiPaolo",
    "seniorityDate": "2007-11-07T05:00:00.000Z",
    "promotionDate": "2007-11-07T05:00:00.000Z",
    "hireDate": "2007-11-07T05:00:00.000Z",
    "listNumber": 7921,
    "chartDates": [ {"date":"2018-06-04T04:00:00.000Z"}, {"date":"2018-06-12T04:00:00.000Z"}, {"date":"2018-06-20T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2629",
    "qualifications" : [ "EZP", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-296-8490"
    }
  },
  {
    "chartNum": "SW-11",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "106235",
    "name": "D. Pulliza",
    "seniorityDate": "2000-07-23T04:00:00.000Z",
    "promotionDate": "2000-07-23T04:00:00.000Z",
    "hireDate": "2000-07-24T04:00:00.000Z",
    "listNumber": 2480,
    "chartDates": [ {"date":"2018-06-04T04:00:00.000Z"}, {"date":"2018-06-12T04:00:00.000Z"}, {"date":"2018-06-20T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3196",
    "qualifications" : [ "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-491-6704"
    }
  },
  {
    "chartNum": "SW-11",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "133131",
    "name": "D. Zelarayan",
    "seniorityDate": "2013-10-13T05:00:00.000Z",
    "promotionDate": "2013-10-13T05:00:00.000Z",
    "hireDate": "2013-10-14T05:00:00.000Z",
    "listNumber": 3316,
    "chartDates": [ {"date":"2018-06-04T04:00:00.000Z"}, {"date":"2018-06-12T04:00:00.000Z"}, {"date":"2018-06-20T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3589",
    "qualifications" : [ "EZP", "FEL", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-989-4771"
    }
  },
  {
    "chartNum": "SW-11",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "109590",
    "name": "L. Gasparino",
    "seniorityDate": "2015-07-12T05:00:00.000Z",
    "promotionDate": "2015-07-12T05:00:00.000Z",
    "hireDate": "2015-07-12T05:00:00.000Z",
    "listNumber": 1477,
    "chartDates": [ {"date":"2018-06-04T04:00:00.000Z"}, {"date":"2018-06-12T04:00:00.000Z"}, {"date":"2018-06-20T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2635",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-626-3232"
    }
  },
  {
    "chartNum": "F3",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "180901",
    "name": "B. Goldstein",
    "seniorityDate": "2013-08-18T05:00:00.000Z",
    "promotionDate": "2013-08-18T05:00:00.000Z",
    "hireDate": "2013-08-19T05:00:00.000Z",
    "listNumber": 1116,
    "chartDates": [ {"date":"2018-06-05T04:00:00.000Z","ccFrom":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-13T04:00:00.000Z"}, {"date":"2018-06-21T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1825",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-145-8080"
    }
  },
  {
    "chartNum": "F3",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "182473",
    "name": "E. Blake",
    "seniorityDate": "2006-11-12T06:00:00.000Z",
    "promotionDate": "2006-11-12T06:00:00.000Z",
    "hireDate": "2006-11-13T06:00:00.000Z",
    "listNumber": 6608,
    "chartDates": [ {"date":"2018-06-05T04:00:00.000Z"}, {"date":"2018-06-13T04:00:00.000Z"}, {"date":"2018-06-21T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1865",
    "qualifications" : [ "EZP", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-834-3131"
    }
  },
  {
    "chartNum": "F3",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "158073",
    "name": "J. James",
    "seniorityDate": "2012-09-23T04:00:00.000Z",
    "promotionDate": "2012-09-23T04:00:00.000Z",
    "hireDate": "2012-09-24T04:00:00.000Z",
    "listNumber": 2236,
    "chartDates": [ {"date":"2018-06-05T04:00:00.000Z"}, {"date":"2018-06-13T04:00:00.000Z"}, {"date":"2018-06-21T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1235",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-471-5689"
    }
  },
  {
    "chartNum": "F3",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "177076",
    "name": "J. Matos",
    "seniorityDate": "2004-09-12T04:00:00.000Z",
    "promotionDate": "2004-09-12T04:00:00.000Z",
    "hireDate": "2004-09-13T04:00:00.000Z",
    "listNumber": 1483,
    "chartDates": [ {"date":"2018-06-05T04:00:00.000Z"}, {"date":"2018-06-13T04:00:00.000Z"}, {"date":"2018-06-21T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1026",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-238-4227"
    }
  },
  {
    "chartNum": "F3",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "149270",
    "name": "J. Nicoletti",
    "seniorityDate": "2013-09-15T05:00:00.000Z",
    "promotionDate": "2013-09-15T05:00:00.000Z",
    "hireDate": "2013-09-16T05:00:00.000Z",
    "listNumber": 2896,
    "chartDates": [ {"date":"2018-06-05T04:00:00.000Z"}, {"date":"2018-06-13T04:00:00.000Z"}, {"date":"2018-06-21T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1055",
    "qualifications" : [ "EZP", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-535-2665"
    }
  },
  {
    "chartNum": "F3",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "135969",
    "name": "M. Sage",
    "seniorityDate": "2004-05-02T04:00:00.000Z",
    "promotionDate": "2004-05-02T04:00:00.000Z",
    "hireDate": "2004-05-03T04:00:00.000Z",
    "listNumber": 849,
    "chartDates": [ {"date":"2018-06-05T04:00:00.000Z"}, {"date":"2018-06-13T04:00:00.000Z"}, {"date":"2018-06-21T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1095",
    "qualifications" : [ "EZP" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-173-3867"
    }
  },
  {
    "chartNum": "SW-12",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "189392",
    "name": "A. Guttilla",
    "seniorityDate": "2005-12-04T06:00:00.000Z",
    "promotionDate": "2005-12-04T06:00:00.000Z",
    "hireDate": "2005-12-05T06:00:00.000Z",
    "listNumber": 4786,
    "chartDates": [ {"date":"2018-06-05T04:00:00.000Z"}, {"date":"2018-06-13T04:00:00.000Z"}, {"date":"2018-06-21T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1755",
    "qualifications" : [ "EZP", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-770-9413"
    }
  },
  {
    "chartNum": "SW-12",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "198609",
    "name": "D. Townsend",
    "seniorityDate": "2015-10-04T05:00:00.000Z",
    "promotionDate": "2015-10-04T05:00:00.000Z",
    "hireDate": "2015-10-04T05:00:00.000Z",
    "listNumber": 5150,
    "chartDates": [ {"date":"2018-06-05T04:00:00.000Z"}, {"date":"2018-06-13T04:00:00.000Z"}, {"date":"2018-06-21T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2413",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-303-2380"
    }
  },
  {
    "chartNum": "SW-12",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "169045",
    "name": "J. Machado",
    "seniorityDate": "2006-08-20T04:00:00.000Z",
    "promotionDate": "2006-08-20T04:00:00.000Z",
    "hireDate": "1992-05-04T04:00:00.000Z",
    "listNumber": 5005,
    "chartDates": [ {"date":"2018-06-05T04:00:00.000Z"}, {"date":"2018-06-13T04:00:00.000Z"}, {"date":"2018-06-21T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3626",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-722-8001"
    }
  },
  {
    "chartNum": "SW-12",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "121356",
    "name": "T. Emigholz",
    "seniorityDate": "2017-07-09T04:00:00.000Z",
    "promotionDate": "2017-07-09T04:00:00.000Z",
    "hireDate": "2017-07-09T04:00:00.000Z",
    "listNumber": 1043,
    "chartDates": [ {"date":"2018-06-05T04:00:00.000Z"}, {"date":"2018-06-13T04:00:00.000Z"}, {"date":"2018-06-21T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1828",
    "qualifications" : [ "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-336-1409"
    }
  },
  {
    "chartNum": "SW-2",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "159833",
    "name": "B. Butingaro",
    "seniorityDate": "2004-10-24T05:00:00.000Z",
    "promotionDate": "2004-10-24T05:00:00.000Z",
    "hireDate": "2004-10-25T05:00:00.000Z",
    "listNumber": 2020,
    "chartDates": [ {"date":"2018-06-05T04:00:00.000Z"}, {"date":"2018-06-13T04:00:00.000Z"}, {"date":"2018-06-21T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1849",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-450-8195"
    }
  },
  {
    "chartNum": "SW-2",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "125535",
    "name": "G. Licata",
    "seniorityDate": "2012-09-23T05:00:00.000Z",
    "promotionDate": "2012-09-23T05:00:00.000Z",
    "hireDate": "2012-09-24T05:00:00.000Z",
    "listNumber": 2261,
    "chartDates": [ {"date":"2018-06-05T04:00:00.000Z"}, {"date":"2018-06-13T04:00:00.000Z"}, {"date":"2018-06-21T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2280",
    "qualifications" : [ "EZP", "FEL", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-534-7237"
    }
  },
  {
    "chartNum": "SW-2",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "178008",
    "name": "J. Angulo",
    "seniorityDate": "2016-09-18T04:00:00.000Z",
    "promotionDate": "2016-09-18T04:00:00.000Z",
    "hireDate": "2016-09-18T04:00:00.000Z",
    "listNumber": 774,
    "chartDates": [ {"date":"2018-06-05T04:00:00.000Z"}, {"date":"2018-06-13T04:00:00.000Z"}, {"date":"2018-06-21T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3396",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-863-9392"
    }
  },
  {
    "chartNum": "SW-2",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "190737",
    "name": "J. Paladine",
    "seniorityDate": "1999-10-17T05:00:00.000Z",
    "promotionDate": "1999-10-17T05:00:00.000Z",
    "hireDate": "1999-10-18T05:00:00.000Z",
    "listNumber": 656,
    "chartDates": [ {"date":"2018-06-05T04:00:00.000Z"}, {"date":"2018-06-13T04:00:00.000Z"}, {"date":"2018-06-21T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2396",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-805-8577"
    }
  },
  {
    "chartNum": "SW-21",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "130967",
    "name": "C. Steward",
    "seniorityDate": "1995-07-30T04:00:00.000Z",
    "promotionDate": "1995-07-30T04:00:00.000Z",
    "hireDate": "1995-07-31T04:00:00.000Z",
    "listNumber": 610,
    "chartDates": [ {"date":"2018-06-05T04:00:00.000Z"}, {"date":"2018-06-13T04:00:00.000Z"}, {"date":"2018-06-21T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3497",
    "qualifications" : [ "EZP" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-502-2234"
    }
  },
  {
    "chartNum": "SW-21",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "140494",
    "name": "R. Communiello",
    "seniorityDate": "2001-04-15T05:00:00.000Z",
    "promotionDate": "2001-04-15T05:00:00.000Z",
    "hireDate": "2001-04-16T05:00:00.000Z",
    "listNumber": 3588,
    "chartDates": [ {"date":"2018-06-05T04:00:00.000Z"}, {"date":"2018-06-13T04:00:00.000Z"}, {"date":"2018-06-21T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1847",
    "qualifications" : [ "EZP", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-495-6311"
    }
  },
  {
    "chartNum": "SW-21",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "186846",
    "name": "S. Omalley",
    "seniorityDate": "2007-09-09T04:00:00.000Z",
    "promotionDate": "2007-09-09T04:00:00.000Z",
    "hireDate": "2007-09-10T04:00:00.000Z",
    "listNumber": 6887,
    "chartDates": [ {"date":"2018-06-05T04:00:00.000Z"}, {"date":"2018-06-13T04:00:00.000Z"}, {"date":"2018-06-21T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1084",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-432-3631"
    }
  },
  {
    "chartNum": "F4",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "165175",
    "name": "C. Arthur",
    "seniorityDate": "2005-05-31T05:00:00.000Z",
    "promotionDate": "2005-05-31T05:00:00.000Z",
    "hireDate": "2005-05-31T05:00:00.000Z",
    "listNumber": 1881.05,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z", "ccFrom":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1005",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-338-5361"
    }
  },
  {
    "chartNum": "F4",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "101592",
    "name": "C. Middleton",
    "seniorityDate": "2015-07-12T04:00:00.000Z",
    "promotionDate": "2015-07-12T04:00:00.000Z",
    "hireDate": "2015-07-12T04:00:00.000Z",
    "listNumber": 4206,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1395",
    "qualifications" : [ "EZP" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-922-5516"
    }
  },
  {
    "chartNum": "F4",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "134392",
    "name": "J. Long",
    "seniorityDate": "2011-09-11T04:00:00.000Z",
    "promotionDate": "2011-09-11T04:00:00.000Z",
    "hireDate": "2011-09-12T04:00:00.000Z",
    "listNumber": 691,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2158",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-131-2162"
    }
  },
  {
    "chartNum": "F4",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "115860",
    "name": "J. McGough",
    "seniorityDate": "2013-08-18T05:00:00.000Z",
    "promotionDate": "2013-08-18T05:00:00.000Z",
    "hireDate": "2013-08-19T05:00:00.000Z",
    "listNumber": 1194,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1137",
    "qualifications" : [ "EZP", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-295-5852"
    }
  },
  {
    "chartNum": "F4",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "136492",
    "name": "N. Radenberg",
    "seniorityDate": "2017-07-09T04:00:00.000Z",
    "promotionDate": "2017-07-09T04:00:00.000Z",
    "hireDate": "2017-07-09T04:00:00.000Z",
    "listNumber": 1096,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1136",
    "qualifications" : [ "EZP" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-221-7922"
    }
  },
  {
    "chartNum": "F4",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "109482",
    "name": "P. Yohay",
    "seniorityDate": "2017-07-09T04:00:00.000Z",
    "promotionDate": "2017-07-09T04:00:00.000Z",
    "hireDate": "2017-07-09T04:00:00.000Z",
    "listNumber": 844.05,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3867",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-503-6649"
    }
  },
  {
    "chartNum": "SW-13",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "185377",
    "name": "A. Douglas",
    "seniorityDate": "2001-09-16T05:00:00.000Z",
    "promotionDate": "2001-09-16T05:00:00.000Z",
    "hireDate": "2001-04-08T05:00:00.000Z",
    "listNumber": 4073,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3131",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-844-8876"
    }
  },
  {
    "chartNum": "SW-13",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "181193",
    "name": "J. Gabino",
    "seniorityDate": "2004-03-21T05:00:00.000Z",
    "promotionDate": "2004-03-21T05:00:00.000Z",
    "hireDate": "2004-03-22T05:00:00.000Z",
    "listNumber": 174,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1681",
    "qualifications" : [ "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-626-7679"
    }
  },
  {
    "chartNum": "SW-13",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "120572",
    "name": "L. Bush",
    "seniorityDate": "2000-08-20T05:00:00.000Z",
    "promotionDate": "2000-08-20T05:00:00.000Z",
    "hireDate": "2000-08-21T05:00:00.000Z",
    "listNumber": 3000,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1965",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-926-1401"
    }
  },
  {
    "chartNum": "SW-13",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "168235",
    "name": "S. McGregor",
    "seniorityDate": "2017-09-10T04:00:00.000Z",
    "promotionDate": "2017-09-10T04:00:00.000Z",
    "hireDate": "2017-09-10T04:00:00.000Z",
    "listNumber": 1572,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1002",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-483-6275"
    }
  },
  {
    "chartNum": "SW-22",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "171577",
    "name": "J. Dennison",
    "seniorityDate": "1995-08-27T05:00:00.000Z",
    "promotionDate": "1995-08-27T05:00:00.000Z",
    "hireDate": "1995-08-28T05:00:00.000Z",
    "listNumber": 2478,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2199",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-617-4478"
    }
  },
  {
    "chartNum": "SW-22",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "147445",
    "name": "P. Bulgar",
    "seniorityDate": "1993-09-07T05:00:00.000Z",
    "promotionDate": "1993-09-07T05:00:00.000Z",
    "hireDate": "1993-09-07T05:00:00.000Z",
    "listNumber": 1586,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3756",
    "qualifications" : [ "EZP", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-581-8214"
    }
  },
  {
    "chartNum": "SW-22",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "125235",
    "name": "S. Kurtovic",
    "seniorityDate": "2005-11-13T05:00:00.000Z",
    "promotionDate": "2005-11-13T05:00:00.000Z",
    "hireDate": "2005-11-14T05:00:00.000Z",
    "listNumber": 4185,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3534",
    "qualifications" : [ "EZP" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-571-1111"
    }
  },
  {
    "chartNum": "SW-3",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "103678",
    "name": "F. Calamanco 3rd",
    "seniorityDate": "2004-10-24T04:00:00.000Z",
    "promotionDate": "2004-10-24T04:00:00.000Z",
    "hireDate": "2004-10-25T04:00:00.000Z",
    "listNumber": 1995,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1964",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-574-2276"
    }
  },
  {
    "chartNum": "SW-3",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "123070",
    "name": "F. Stephens",
    "seniorityDate": "2006-11-12T06:00:00.000Z",
    "promotionDate": "2006-11-12T06:00:00.000Z",
    "hireDate": "2006-11-13T06:00:00.000Z",
    "listNumber": 2858,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3680",
    "qualifications" : [ "EZP", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-490-1244"
    }
  },
  {
    "chartNum": "SW-3",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "158976",
    "name": "J. Lasekan",
    "seniorityDate": "2000-04-02T05:00:00.000Z",
    "promotionDate": "2000-04-02T05:00:00.000Z",
    "hireDate": "2000-04-03T05:00:00.000Z",
    "listNumber": 1738,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2893",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-396-8979"
    }
  },
  {
    "chartNum": "SW-3",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "157367",
    "name": "S. Joe",
    "seniorityDate": "1995-08-27T05:00:00.000Z",
    "promotionDate": "1995-08-27T05:00:00.000Z",
    "hireDate": "1995-08-28T05:00:00.000Z",
    "listNumber": 2072,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1663",
    "qualifications" : [ "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-355-1444"
    }
  },
  {
    "chartNum": "SW-7",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "128554",
    "name": "C. Morales",
    "seniorityDate": "2004-10-24T05:00:00.000Z",
    "promotionDate": "2004-10-24T05:00:00.000Z",
    "hireDate": "2004-10-25T05:00:00.000Z",
    "listNumber": 1886,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3630",
    "qualifications" : [ "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-167-2181"
    }
  },
  {
    "chartNum": "SW-7",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "197581",
    "name": "J. Camargo",
    "seniorityDate": "2001-05-20T05:00:00.000Z",
    "promotionDate": "2001-05-20T05:00:00.000Z",
    "hireDate": "2001-05-21T05:00:00.000Z",
    "listNumber": 3767,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2770",
    "qualifications" : [ "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-810-2731"
    }
  },
  {
    "chartNum": "SW-7",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "149433",
    "name": "J. Diaz",
    "seniorityDate": "2014-07-20T04:00:00.000Z",
    "promotionDate": "2014-07-20T04:00:00.000Z",
    "hireDate": "2014-07-21T04:00:00.000Z",
    "listNumber": 3450,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3412",
    "qualifications" : [ "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-615-7914"
    }
  },
  {
    "chartNum": "SW-7",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "111319",
    "name": "R. Casimiro",
    "seniorityDate": "2015-10-04T05:00:00.000Z",
    "promotionDate": "2015-10-04T05:00:00.000Z",
    "hireDate": "2015-10-04T05:00:00.000Z",
    "listNumber": 5158,
    "chartDates": [ {"date":"2018-06-06T04:00:00.000Z"}, {"date":"2018-06-14T04:00:00.000Z"}, {"date":"2018-06-22T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2107",
    "qualifications" : [ "EZP" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-835-8848"
    }
  },
  {
    "chartNum": "F5",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "110356",
    "name": "A. Travieso",
    "seniorityDate": "2007-09-09T04:00:00.000Z",
    "promotionDate": "2007-09-09T04:00:00.000Z",
    "hireDate": "2007-09-10T04:00:00.000Z",
    "listNumber": 6988,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z", "ccFrom":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2555",
    "qualifications" : [ "EZP" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-129-5944"
    }
  },
  {
    "chartNum": "F5",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "199259",
    "name": "C. Steele",
    "seniorityDate": "2017-09-10T04:00:00.000Z",
    "promotionDate": "2017-09-10T04:00:00.000Z",
    "hireDate": "2017-09-11T04:00:00.000Z",
    "listNumber": 1446,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2808",
    "qualifications" : [ "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-602-5897"
    }
  },
  {
    "chartNum": "F5",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "153037",
    "name": "G. Ramirez",
    "seniorityDate": "2012-09-23T04:00:00.000Z",
    "promotionDate": "2012-09-23T04:00:00.000Z",
    "hireDate": "2012-09-24T04:00:00.000Z",
    "listNumber": 2223,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2398",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-783-7768"
    }
  },
  {
    "chartNum": "F5",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "167903",
    "name": "K. Hall",
    "seniorityDate": "2014-08-17T05:00:00.000Z",
    "promotionDate": "2014-08-17T05:00:00.000Z",
    "hireDate": "2014-08-18T05:00:00.000Z",
    "listNumber": 4383,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3325",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-370-5079"
    }
  },
  {
    "chartNum": "F5",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "130740",
    "name": "R. Futrell",
    "seniorityDate": "1999-12-26T06:00:00.000Z",
    "promotionDate": "1999-12-26T06:00:00.000Z",
    "hireDate": "1999-12-27T06:00:00.000Z",
    "listNumber": 863,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2032",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-164-6322"
    }
  },
  {
    "chartNum": "F5",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "108958",
    "name": "S. Lopez",
    "seniorityDate": "2012-10-21T05:00:00.000Z",
    "promotionDate": "2012-10-21T05:00:00.000Z",
    "hireDate": "2012-10-22T05:00:00.000Z",
    "listNumber": 2512,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3448",
    "qualifications" : [ "EZP" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-619-7238"
    }
  },
  {
    "chartNum": "SW-14",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "164991",
    "name": "D. Holmes",
    "seniorityDate": "2008-08-10T04:00:00.000Z",
    "promotionDate": "2008-08-10T04:00:00.000Z",
    "hireDate": "2008-08-11T04:00:00.000Z",
    "listNumber": 448,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2091",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-782-5469"
    }
  },
  {
    "chartNum": "SW-14",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "102504",
    "name": "G. Lohse",
    "seniorityDate": "2008-08-10T04:00:00.000Z",
    "promotionDate": "2008-08-10T04:00:00.000Z",
    "hireDate": "2006-12-21T05:00:00.000Z",
    "listNumber": 334,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1621",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-378-3468"
    }
  },
  {
    "chartNum": "SW-14",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "130873",
    "name": "J. Santana",
    "seniorityDate": "2016-07-17T05:00:00.000Z",
    "promotionDate": "2016-07-17T05:00:00.000Z",
    "hireDate": "2016-07-17T05:00:00.000Z",
    "listNumber": 173,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3338",
    "qualifications" : [ "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-772-2321"
    }
  },
  {
    "chartNum": "SW-14",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "162446",
    "name": "M. O'Neill",
    "seniorityDate": "2011-11-13T06:00:00.000Z",
    "promotionDate": "2011-11-13T06:00:00.000Z",
    "hireDate": "2011-11-14T06:00:00.000Z",
    "listNumber": 1420,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2225",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-917-8246"
    }
  },
  {
    "chartNum": "SW-16",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "148134",
    "name": "A. Martin",
    "seniorityDate": "2017-08-13T04:00:00.000Z",
    "promotionDate": "2017-08-13T04:00:00.000Z",
    "hireDate": "2017-08-13T04:00:00.000Z",
    "listNumber": 1047,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3521",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-935-5084"
    }
  },
  {
    "chartNum": "SW-16",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "162937",
    "name": "C. Johnson",
    "seniorityDate": "2004-03-21T05:00:00.000Z",
    "promotionDate": "2004-03-21T05:00:00.000Z",
    "hireDate": "2004-03-22T05:00:00.000Z",
    "listNumber": 516,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2818",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-770-2014"
    }
  },
  {
    "chartNum": "SW-16",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "111440",
    "name": "F. Perrone",
    "seniorityDate": "2014-07-20T04:00:00.000Z",
    "promotionDate": "2014-07-20T04:00:00.000Z",
    "hireDate": "2014-07-21T04:00:00.000Z",
    "listNumber": 3615,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2439",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-476-5537"
    }
  },
  {
    "chartNum": "SW-16",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "106741",
    "name": "K. Mann",
    "seniorityDate": "1999-10-03T05:00:00.000Z",
    "promotionDate": "1999-10-03T05:00:00.000Z",
    "hireDate": "1999-10-04T05:00:00.000Z",
    "listNumber": 312,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3166",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-321-9920"
    }
  },
  {
    "chartNum": "SW-18",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "128324",
    "name": "D. Garcia",
    "seniorityDate": "2001-09-16T05:00:00.000Z",
    "promotionDate": "2001-09-16T05:00:00.000Z",
    "hireDate": "2001-09-17T05:00:00.000Z",
    "listNumber": 4206,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2759",
    "qualifications" : [ "EZP", "FEL", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-190-1785"
    }
  },
  {
    "chartNum": "SW-18",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "195432",
    "name": "H. Caban",
    "seniorityDate": "2013-11-10T06:00:00.000Z",
    "promotionDate": "2013-11-10T06:00:00.000Z",
    "hireDate": "2013-11-11T06:00:00.000Z",
    "listNumber": 3784,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3058",
    "qualifications" : [ "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-690-3930"
    }
  },
  {
    "chartNum": "SW-18",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "107704",
    "name": "R. Wranovics",
    "seniorityDate": "2017-07-09T04:00:00.000Z",
    "promotionDate": "2017-07-09T04:00:00.000Z",
    "hireDate": "2017-07-09T04:00:00.000Z",
    "listNumber": 1042,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1191",
    "qualifications" : [ "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-634-8577"
    }
  },
  {
    "chartNum": "SW-18",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "194995",
    "name": "V. Langston",
    "seniorityDate": "2015-09-06T05:00:00.000Z",
    "promotionDate": "2015-09-06T05:00:00.000Z",
    "hireDate": "2015-09-06T05:00:00.000Z",
    "listNumber": 6343,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2185",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-528-9042"
    }
  },
  {
    "chartNum": "SW-23",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "136195",
    "name": "C. Smith",
    "seniorityDate": "1995-08-27T05:00:00.000Z",
    "promotionDate": "1995-08-27T05:00:00.000Z",
    "hireDate": "1991-12-09T06:00:00.000Z",
    "listNumber": 2146,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1683",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-931-1778"
    }
  },
  {
    "chartNum": "SW-23",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "110655",
    "name": "E. Jordan",
    "seniorityDate": "1995-08-27T05:00:00.000Z",
    "promotionDate": "1995-08-27T05:00:00.000Z",
    "hireDate": "1991-07-08T05:00:00.000Z",
    "listNumber": 2350,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2907",
    "qualifications" : [ "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-698-7406"
    }
  },
  {
    "chartNum": "SW-23",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "141474",
    "name": "F. Aguilar",
    "seniorityDate": "2006-05-21T04:00:00.000Z",
    "promotionDate": "2006-05-21T04:00:00.000Z",
    "hireDate": "2006-05-22T04:00:00.000Z",
    "listNumber": 4177,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1398",
    "qualifications" : [ "EZP", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-630-5367"
    }
  },
  {
    "chartNum": "SW-4",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "191986",
    "name": "G. Baer",
    "seniorityDate": "2004-10-03T05:00:00.000Z",
    "promotionDate": "2004-10-03T05:00:00.000Z",
    "hireDate": "2004-10-04T05:00:00.000Z",
    "listNumber": 1785,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3760",
    "qualifications" : [ "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-983-8360"
    }
  },
  {
    "chartNum": "SW-4",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "108513",
    "name": "H. Torres",
    "seniorityDate": "2015-09-06T04:00:00.000Z",
    "promotionDate": "2015-09-06T04:00:00.000Z",
    "hireDate": "2015-09-06T04:00:00.000Z",
    "listNumber": 6282,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1026",
    "qualifications" : [ "EZP", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-554-6777"
    }
  },
  {
    "chartNum": "SW-4",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "124364",
    "name": "I. Irizarry",
    "seniorityDate": "2000-08-06T05:00:00.000Z",
    "promotionDate": "2000-08-06T05:00:00.000Z",
    "hireDate": "2000-08-07T05:00:00.000Z",
    "listNumber": 2958,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2987",
    "qualifications" : [ "EZP" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-243-9485"
    }
  },
  {
    "chartNum": "SW-4",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "199874",
    "name": "K. Pascocello",
    "seniorityDate": "2005-05-08T05:00:00.000Z",
    "promotionDate": "2005-05-08T05:00:00.000Z",
    "hireDate": "2005-05-09T05:00:00.000Z",
    "listNumber": 1460,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3404",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-843-7713"
    }
  },
  {
    "chartNum": "SW-8",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "173650",
    "name": "D. Medina",
    "seniorityDate": "2007-11-07T06:00:00.000Z",
    "promotionDate": "2007-11-07T06:00:00.000Z",
    "hireDate": "2005-04-11T05:00:00.000Z",
    "listNumber": 6729,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3160",
    "qualifications" : [ "EZP", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-130-6326"
    }
  },
  {
    "chartNum": "SW-8",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "199840",
    "name": "G. Salley",
    "seniorityDate": "1993-09-07T05:00:00.000Z",
    "promotionDate": "1993-09-07T05:00:00.000Z",
    "hireDate": "1993-09-07T05:00:00.000Z",
    "listNumber": 1574,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3222",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-803-1866"
    }
  },
  {
    "chartNum": "SW-8",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "121316",
    "name": "J. Caraballo",
    "seniorityDate": "1998-12-10T06:00:00.000Z",
    "promotionDate": "1998-12-10T06:00:00.000Z",
    "hireDate": "1998-12-10T06:00:00.000Z",
    "listNumber": 4160,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1298",
    "qualifications" : [ "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-518-7124"
    }
  },
  {
    "chartNum": "SW-8",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "197628",
    "name": "T. Skehill",
    "seniorityDate": "2012-09-23T04:00:00.000Z",
    "promotionDate": "2012-09-23T04:00:00.000Z",
    "hireDate": "2012-09-24T04:00:00.000Z",
    "listNumber": 1096,
    "chartDates": [ {"date":"2018-06-07T04:00:00.000Z"}, {"date":"2018-06-15T04:00:00.000Z"}, {"date":"2018-06-23T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3770",
    "qualifications" : [ "EZP", "FEL", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-206-1239"
    }
  },
  {
    "chartNum": "F6",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "131350",
    "name": "D. Utsey",
    "seniorityDate": "2001-09-16T05:00:00.000Z",
    "promotionDate": "2001-09-16T05:00:00.000Z",
    "hireDate": "1990-03-12T05:00:00.000Z",
    "listNumber": 4009,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z", "ccFrom":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3269",
    "qualifications" : [ "EZP", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-387-4994"
    }
  },
  {
    "chartNum": "F6",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "110550",
    "name": "M. Cassano",
    "seniorityDate": "2017-09-10T04:00:00.000Z",
    "promotionDate": "2017-09-10T04:00:00.000Z",
    "hireDate": "2017-09-10T04:00:00.000Z",
    "listNumber": 1510,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2051",
    "qualifications" : [ "EZP", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-243-7790"
    }
  },
  {
    "chartNum": "F6",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "133125",
    "name": "M. Williams",
    "seniorityDate": "2017-08-13T04:00:00.000Z",
    "promotionDate": "2017-08-13T04:00:00.000Z",
    "hireDate": "2017-08-13T04:00:00.000Z",
    "listNumber": 996,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3888",
    "qualifications" : [ "EZP" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-872-4833"
    }
  },
  {
    "chartNum": "F6",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "163028",
    "name": "R. Peterson",
    "seniorityDate": "2001-09-16T04:00:00.000Z",
    "promotionDate": "2001-09-16T04:00:00.000Z",
    "hireDate": "1987-02-09T05:00:00.000Z",
    "listNumber": 3874,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3578",
    "qualifications" : [ "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-473-2196"
    }
  },
  {
    "chartNum": "F6",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "166902",
    "name": "R. Savary",
    "seniorityDate": "2007-11-07T06:00:00.000Z",
    "promotionDate": "2007-11-07T06:00:00.000Z",
    "hireDate": "2007-11-07T06:00:00.000Z",
    "listNumber": 8258,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1175",
    "qualifications" : [ "EZP", "FEL", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-426-7565"
    }
  },
  {
    "chartNum": "F6",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "199623",
    "name": "T. Burns",
    "seniorityDate": "2001-09-16T05:00:00.000Z",
    "promotionDate": "2001-09-16T05:00:00.000Z",
    "hireDate": "2001-07-13T05:00:00.000Z",
    "listNumber": 4238,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2011",
    "qualifications" : [ "EZP", "FEL", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-390-3611"
    }
  },
  {
    "chartNum": "SW-17",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "196598",
    "name": "D. Hulkower",
    "seniorityDate": "2016-08-21T05:00:00.000Z",
    "promotionDate": "2016-08-21T05:00:00.000Z",
    "hireDate": "2016-08-21T05:00:00.000Z",
    "listNumber": 318,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3922",
    "qualifications" : [ "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-162-4376"
    }
  },
  {
    "chartNum": "SW-17",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "198946",
    "name": "D. Tirado",
    "seniorityDate": "2006-10-15T04:00:00.000Z",
    "promotionDate": "2006-10-15T04:00:00.000Z",
    "hireDate": "2006-10-16T04:00:00.000Z",
    "listNumber": 5998,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3770",
    "qualifications" : [ "EZP", "FEL", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-921-3766"
    }
  },
  {
    "chartNum": "SW-17",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "128675",
    "name": "J. Santiago Vidal",
    "seniorityDate": "2017-07-09T04:00:00.000Z",
    "promotionDate": "2017-07-09T04:00:00.000Z",
    "hireDate": "2017-07-09T04:00:00.000Z",
    "listNumber": 1040,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1664",
    "qualifications" : [ "EZP", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-896-7528"
    }
  },
  {
    "chartNum": "SW-17",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "168073",
    "name": "J. Vaskis",
    "seniorityDate": "2016-09-18T04:00:00.000Z",
    "promotionDate": "2016-09-18T04:00:00.000Z",
    "hireDate": "2016-09-18T04:00:00.000Z",
    "listNumber": 740,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1648",
    "qualifications" : [ "EZP" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-508-2408"
    }
  },
  {
    "chartNum": "SW-19",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "111480",
    "name": "A. Diaz",
    "seniorityDate": "2007-09-09T04:00:00.000Z",
    "promotionDate": "2007-09-09T04:00:00.000Z",
    "hireDate": "2007-09-10T04:00:00.000Z",
    "listNumber": 6626.05,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2359",
    "qualifications" : [ "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-712-8688"
    }
  },
  {
    "chartNum": "SW-19",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "196391",
    "name": "D. Derwin",
    "seniorityDate": "2013-08-18T05:00:00.000Z",
    "promotionDate": "2013-08-18T05:00:00.000Z",
    "hireDate": "2013-08-19T05:00:00.000Z",
    "listNumber": 2178,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1719",
    "qualifications" : [ "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-763-7242"
    }
  },
  {
    "chartNum": "SW-19",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "171513",
    "name": "E. Rivera",
    "seniorityDate": "2005-11-13T06:00:00.000Z",
    "promotionDate": "2005-11-13T06:00:00.000Z",
    "hireDate": "2005-11-14T06:00:00.000Z",
    "listNumber": 4311,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3141",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-813-8688"
    }
  },
  {
    "chartNum": "SW-19",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "187727",
    "name": "P. Masso",
    "seniorityDate": "2016-08-21T05:00:00.000Z",
    "promotionDate": "2016-08-21T05:00:00.000Z",
    "hireDate": "2016-08-21T05:00:00.000Z",
    "listNumber": 338,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2735",
    "qualifications" : [ "EZP" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-530-2254"
    }
  },
  {
    "chartNum": "SW-24",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "116735",
    "name": "F. Orofino",
    "seniorityDate": "2013-11-10T05:00:00.000Z",
    "promotionDate": "2013-11-10T05:00:00.000Z",
    "hireDate": "2013-11-11T05:00:00.000Z",
    "listNumber": 3644.05,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2430",
    "qualifications" : [ "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-241-5583"
    }
  },
  {
    "chartNum": "SW-24",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "114453",
    "name": "J. Damiano",
    "seniorityDate": "1999-10-03T05:00:00.000Z",
    "promotionDate": "1999-10-03T05:00:00.000Z",
    "hireDate": "1999-10-04T05:00:00.000Z",
    "listNumber": 329,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1830",
    "qualifications" : [ "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-227-5030"
    }
  },
  {
    "chartNum": "SW-24",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "165612",
    "name": "R. Lenhart",
    "seniorityDate": "2005-11-13T05:00:00.000Z",
    "promotionDate": "2005-11-13T05:00:00.000Z",
    "hireDate": "2005-11-14T05:00:00.000Z",
    "listNumber": 4306,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2332",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-760-5488"
    }
  },
  {
    "chartNum": "SW-6",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "150672",
    "name": "D. Cay",
    "seniorityDate": "2014-08-17T04:00:00.000Z",
    "promotionDate": "2014-08-17T04:00:00.000Z",
    "hireDate": "2014-08-18T04:00:00.000Z",
    "listNumber": 4365,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2371",
    "qualifications" : [ "EZP" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-604-3255"
    }
  },
  {
    "chartNum": "SW-6",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "106147",
    "name": "E. Messina",
    "seniorityDate": "2017-08-13T04:00:00.000Z",
    "promotionDate": "2017-08-13T04:00:00.000Z",
    "hireDate": "2017-08-13T04:00:00.000Z",
    "listNumber": 964,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3325",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-947-4157"
    }
  },
  {
    "chartNum": "SW-6",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "127954",
    "name": "J. Hunt",
    "seniorityDate": "2005-05-31T04:00:00.000Z",
    "promotionDate": "2005-05-31T04:00:00.000Z",
    "hireDate": "2005-05-31T04:00:00.000Z",
    "listNumber": 2169,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1674",
    "qualifications" : [ "EZP", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-950-9570"
    }
  },
  {
    "chartNum": "SW-6",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "109107",
    "name": "M. Caldwell",
    "seniorityDate": "2000-07-09T05:00:00.000Z",
    "promotionDate": "2000-07-09T05:00:00.000Z",
    "hireDate": "2000-07-10T05:00:00.000Z",
    "listNumber": 2196,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3380",
    "qualifications" : [ "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-416-2275"
    }
  },
  {
    "chartNum": "SW-9",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "107288",
    "name": "D. Saenz",
    "seniorityDate": "2004-05-23T05:00:00.000Z",
    "promotionDate": "2004-05-23T05:00:00.000Z",
    "hireDate": "1995-11-15T06:00:00.000Z",
    "listNumber": 987,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3955",
    "qualifications" : [ "EZP", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-803-9846"
    }
  },
  {
    "chartNum": "SW-9",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "112583",
    "name": "E. Reinhardt",
    "seniorityDate": "2005-09-11T05:00:00.000Z",
    "promotionDate": "2005-09-11T05:00:00.000Z",
    "hireDate": "2005-09-12T05:00:00.000Z",
    "listNumber": 3732,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2789",
    "qualifications" : [ "EZP", "FEL", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-355-4674"
    }
  },
  {
    "chartNum": "SW-9",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "143459",
    "name": "J. Dantuono",
    "seniorityDate": "2014-08-17T04:00:00.000Z",
    "promotionDate": "2014-08-17T04:00:00.000Z",
    "hireDate": "2014-08-18T04:00:00.000Z",
    "listNumber": 3816,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1948",
    "qualifications" : [ "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-292-4774"
    }
  },
  {
    "chartNum": "SW-9",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "132003",
    "name": "R. McCoy",
    "seniorityDate": "2004-03-21T05:00:00.000Z",
    "promotionDate": "2004-03-21T05:00:00.000Z",
    "hireDate": "2004-03-22T05:00:00.000Z",
    "listNumber": 464,
    "chartDates": [ {"date":"2018-06-08T04:00:00.000Z"}, {"date":"2018-06-16T04:00:00.000Z"}, {"date":"2018-06-18T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2788",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-486-2022"
    }
  },
  {
    "chartNum": "F1",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "104697",
    "name": "D. Palermo",
    "seniorityDate": "2017-08-13T04:00:00.000Z",
    "promotionDate": "2017-08-13T04:00:00.000Z",
    "hireDate": "2017-08-13T04:00:00.000Z",
    "listNumber": 1033,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z", "ccFrom":"2018-06-04T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1341",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-111-6925"
    }
  },
  {
    "chartNum": "F1",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "127725",
    "name": "D. Saccone",
    "seniorityDate": "2015-10-04T04:00:00.000Z",
    "promotionDate": "2015-10-04T04:00:00.000Z",
    "hireDate": "2015-10-04T04:00:00.000Z",
    "listNumber": 6194,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1945",
    "qualifications" : [ "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-462-5199"
    }
  },
  {
    "chartNum": "F1",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "135766",
    "name": "E. Backer",
    "seniorityDate": "2017-09-10T04:00:00.000Z",
    "promotionDate": "2017-09-10T04:00:00.000Z",
    "hireDate": "2013-12-23T05:00:00.000Z",
    "listNumber": 1271,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3924",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-908-3558"
    }
  },
  {
    "chartNum": "F1",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "139158",
    "name": "J. Rosiello",
    "seniorityDate": "2016-07-17T04:00:00.000Z",
    "promotionDate": "2016-07-17T04:00:00.000Z",
    "hireDate": "2016-07-17T04:00:00.000Z",
    "listNumber": 174,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3511",
    "qualifications" : [ "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-452-3967"
    }
  },
  {
    "chartNum": "F1",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "117139",
    "name": "L. Fanniel",
    "seniorityDate": "2007-11-07T06:00:00.000Z",
    "promotionDate": "2007-11-07T06:00:00.000Z",
    "hireDate": "2007-11-07T06:00:00.000Z",
    "listNumber": 3371,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1973",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-564-2029"
    }
  },
  {
    "chartNum": "F1",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "194208",
    "name": "M. Distefano",
    "seniorityDate": "2014-08-17T04:00:00.000Z",
    "promotionDate": "2014-08-17T04:00:00.000Z",
    "hireDate": "2014-08-18T04:00:00.000Z",
    "listNumber": 4559,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1606",
    "qualifications" : [ "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-492-5110"
    }
  },
  {
    "chartNum": "SW-10",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "178585",
    "name": "A. Kalayanamitra",
    "seniorityDate": "2014-07-20T05:00:00.000Z",
    "promotionDate": "2014-07-20T05:00:00.000Z",
    "hireDate": "2014-07-21T05:00:00.000Z",
    "listNumber": 3230,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": true,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3383",
    "qualifications" : [ "EZP", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-855-3849"
    }
  },
  {
    "chartNum": "SW-10",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "165764",
    "name": "D. Marsac",
    "seniorityDate": "2011-11-13T06:00:00.000Z",
    "promotionDate": "2011-11-13T06:00:00.000Z",
    "hireDate": "2011-11-14T06:00:00.000Z",
    "listNumber": 1140,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3230",
    "qualifications" : [ "EZP" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-841-8573"
    }
  },
  {
    "chartNum": "SW-10",
    "homeLocation": "MN07",
    "section": "G",
    "reference": "193253",
    "name": "N. Mcdowell",
    "seniorityDate": "2001-09-16T05:00:00.000Z",
    "promotionDate": "2001-09-16T05:00:00.000Z",
    "hireDate": "2001-09-17T05:00:00.000Z",
    "listNumber": 4239,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1280",
    "qualifications" : [ "EZP", "FEL", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-648-2436"
    }
  },
  {
    "chartNum": "SW-10",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "105920",
    "name": "T. Williamson",
    "seniorityDate": "2016-07-17T05:00:00.000Z",
    "promotionDate": "2016-07-17T05:00:00.000Z",
    "hireDate": "2016-07-17T05:00:00.000Z",
    "listNumber": 326,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3141",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-498-8930"
    }
  },
  {
    "chartNum": "SW-15",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "188844",
    "name": "B. Golson",
    "seniorityDate": "2015-10-04T04:00:00.000Z",
    "promotionDate": "2015-10-04T04:00:00.000Z",
    "hireDate": "2015-10-04T04:00:00.000Z",
    "listNumber": 6780,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3939",
    "qualifications" : [ "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-945-6243"
    }
  },
  {
    "chartNum": "SW-15",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "110687",
    "name": "B. Jackson",
    "seniorityDate": "2001-09-16T05:00:00.000Z",
    "promotionDate": "2001-09-16T05:00:00.000Z",
    "hireDate": "2001-09-17T05:00:00.000Z",
    "listNumber": 3936,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2235",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-197-4770"
    }
  },
  {
    "chartNum": "SW-15",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "120575",
    "name": "J. Crespo",
    "seniorityDate": "2005-05-08T04:00:00.000Z",
    "promotionDate": "2005-05-08T04:00:00.000Z",
    "hireDate": "2005-05-09T04:00:00.000Z",
    "listNumber": 461,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3813",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-129-1741"
    }
  },
  {
    "chartNum": "SW-15",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "168202",
    "name": "K. Headley",
    "seniorityDate": "2014-07-20T05:00:00.000Z",
    "promotionDate": "2014-07-20T05:00:00.000Z",
    "hireDate": "2014-07-21T05:00:00.000Z",
    "listNumber": 3953,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1927",
    "qualifications" : [ "EZP", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-909-2651"
    }
  },
  {
    "chartNum": "SW-20",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "131239",
    "name": "A. Alessi",
    "seniorityDate": "2000-03-19T05:00:00.000Z",
    "promotionDate": "2000-03-19T05:00:00.000Z",
    "hireDate": "2000-03-20T05:00:00.000Z",
    "listNumber": 1191,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3762",
    "qualifications" : [ "EZP" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-670-9690"
    }
  },
  {
    "chartNum": "SW-20",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "122276",
    "name": "E. Ustaszewski",
    "seniorityDate": "2004-05-23T05:00:00.000Z",
    "promotionDate": "2004-05-23T05:00:00.000Z",
    "hireDate": "2004-05-24T05:00:00.000Z",
    "listNumber": 1031,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3822",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-917-7110"
    }
  },
  {
    "chartNum": "SW-20",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "188481",
    "name": "K. Thompson",
    "seniorityDate": "1990-03-04T06:00:00.000Z",
    "promotionDate": "1990-03-04T06:00:00.000Z",
    "hireDate": "1990-03-05T06:00:00.000Z",
    "listNumber": 7688,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3977",
    "qualifications" : [ "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-483-6033"
    }
  },
  {
    "chartNum": "SW-20",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "156034",
    "name": "K. Watson",
    "seniorityDate": "2004-05-23T04:00:00.000Z",
    "promotionDate": "2004-05-23T04:00:00.000Z",
    "hireDate": "2004-05-24T04:00:00.000Z",
    "listNumber": 1006,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3799",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-634-9355"
    }
  },
  {
    "chartNum": "SW-25",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "163883",
    "name": "E. Belnavis",
    "seniorityDate": "2015-07-12T05:00:00.000Z",
    "promotionDate": "2015-07-12T05:00:00.000Z",
    "hireDate": "2015-07-12T05:00:00.000Z",
    "listNumber": 4523,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2921",
    "qualifications" : [ "EZP" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-439-9726"
    }
  },
  {
    "chartNum": "SW-25",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "134889",
    "name": "J. Jackson",
    "seniorityDate": "1998-12-10T06:00:00.000Z",
    "promotionDate": "1998-12-10T06:00:00.000Z",
    "hireDate": "1998-12-10T06:00:00.000Z",
    "listNumber": 4144,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3391",
    "qualifications" : [ "EZP", "FEL", "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-957-5027"
    }
  },
  {
    "chartNum": "SW-25",
    "homeLocation": "MN07",
    "section": "3",
    "reference": "112994",
    "name": "N. Rosenfeld",
    "seniorityDate": "1999-09-19T05:00:00.000Z",
    "promotionDate": "1999-09-19T05:00:00.000Z",
    "hireDate": "1999-09-20T05:00:00.000Z",
    "listNumber": 209,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3625",
    "qualifications" : [ "EZP", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-788-9668"
    }
  },
  {
    "chartNum": "SW-5",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "156734",
    "name": "I. Savary",
    "seniorityDate": "2007-11-07T05:00:00.000Z",
    "promotionDate": "2007-11-07T05:00:00.000Z",
    "hireDate": "2007-11-07T05:00:00.000Z",
    "listNumber": 6826,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1924",
    "qualifications" : [ "MB", "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-569-6251"
    }
  },
  {
    "chartNum": "SW-5",
    "homeLocation": "MN07",
    "section": "4",
    "reference": "198341",
    "name": "J. Piguave",
    "seniorityDate": "2016-07-17T04:00:00.000Z",
    "promotionDate": "2016-07-17T04:00:00.000Z",
    "hireDate": "2016-07-17T04:00:00.000Z",
    "listNumber": 397,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "2646",
    "qualifications" : [ "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-720-3903"
    }
  },
  {
    "chartNum": "SW-5",
    "homeLocation": "MN07",
    "section": "1",
    "reference": "109627",
    "name": "R. Miley",
    "seniorityDate": "2006-05-21T05:00:00.000Z",
    "promotionDate": "2006-05-21T05:00:00.000Z",
    "hireDate": "2006-05-22T05:00:00.000Z",
    "listNumber": 4019,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "1686",
    "qualifications" : [ "MB" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-774-5674"
    }
  },
  {
    "chartNum": "SW-5",
    "homeLocation": "MN07",
    "section": "2",
    "reference": "111844",
    "name": "V. Febre",
    "seniorityDate": "1997-07-06T05:00:00.000Z",
    "promotionDate": "1997-07-06T05:00:00.000Z",
    "hireDate": "1997-07-07T05:00:00.000Z",
    "listNumber": 2582,
    "chartDates": [ {"date":"2018-06-09T04:00:00.000Z"}, {"date":"2018-06-11T04:00:00.000Z"}, {"date":"2018-06-19T04:00:00.000Z"} ],
    "chartsMade": 85,
    "isCC": false,
    "isMDA": false,
    "isSick": false,
    "isVC": false,
    "isLodi": false,
    "badgeNumber" : "3187",
    "qualifications" : [ "FEL" ],
    "civilServiceTitle" : "SW",
    "phones" : [ {
    "phone" : "347/983-4809",
    "type" : "HOME"
               } ],
    "addresses" : [ {
    "line1" : "39 CHARLESTON AVENUE",
    "city" : "Staten Island",
    "state" : "NY",
    "postalCode" : "10309",
    "county" : "R",
    "type" : "HOME"
                  } ],
    "hrmsDetail" : {
     "categoryCode" : "A",
     "residenceZone" : 8,
     "residenceDistrict" : "SI03",
     "priorDutyType" : "RD",
     "shift" : "2",
     "address" : {
       "line1" : "39 CHARLESTON AVE",
       "city" : "SI",
       "state" : "NY",
       "postalCode" : "10309"
      },
     "phoneNumber" : "718-118-4254"
    }
  }

];
  
  constructor() { }

}

